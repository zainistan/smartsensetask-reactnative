import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/views/login';
import { Provider } from 'react-redux';
//import { PersistGate } from 'redux-persist/integration/react';
import { store } from './src/store';

const initialState = {};

const reducer = (state = initialState) => {
  return state;
}

export default function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Login />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
