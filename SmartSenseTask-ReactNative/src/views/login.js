import React from 'react';
import { TextInput, View, Button } from 'react-native';
import * as UserActions from '../actions/userActions';
import { connect } from 'react-redux';

const mapDispatchToProps = (dispatch) => {
    return {
        login: (userName, password) => UserActions.login(userName, password, dispatch)
    }
}

class Login extends React.Component {
    render(){
        return(
            <View style={{
                alignContent: "center"
            }}>
                <TextInput placeholder="Enter your username" style={{ padding: 5, marginBottom: 5, borderColor: 'black', borderWidth: 1}} />
                <TextInput secureTextEntry={true} placeholder="Enter your password" style={{ padding: 5, borderColor: 'black', borderWidth: 1}}/>
                <Button title="Login" onPress={() => this.props.login('task', 'football')} />
            </View>
        );
    }
}

export default connect(() => { return {}}, mapDispatchToProps)(Login);