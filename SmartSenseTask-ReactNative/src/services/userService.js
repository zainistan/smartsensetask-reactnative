import React from 'react';
import API from '../../utils/API';

export async function doLogin(username, password) {
    return API.post('/login',
    {
        userName: username,
        password: password
    })
    .then(function (response) {
        return response.data.token;
    })
    .catch(function (error) {
        console.log(error);
    });
}
