import { doLogin } from '../services/userService';

export function login(userName, password, dispatch) {
    return doLogin(userName, password).then(response => {
    // dispatch
    dispatch({
        type:'LOGIN',
        token: response
    })
    })
}