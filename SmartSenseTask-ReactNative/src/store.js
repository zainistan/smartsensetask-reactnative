// Imports: Dependencies
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';
//import AsyncStorage from '@react-native-community/async-storage';

// Imports: Redux
import rootReducer from '../src/reducer/index';

// Middleware: Redux Persist Config
// const persistConfig = {
//   // Root
//   key: 'root',
//   // Storage Method (React Native)
//   storage: AsyncStorage
// };
// // Middleware: Redux Persist Persisted Reducer
// const persistedReducer = persistReducer(persistConfig, rootReducer);
// Redux: Store

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware())
);
// Middleware: Redux Persist Persister
//let persistor = persistStore(store);
// Exports
export {
  store
};