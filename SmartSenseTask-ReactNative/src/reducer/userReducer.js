import * as UserActions from '../actions/userActions';

const INITIAL_STATE = {
    userName: '',
    token: ''
};

const userReducer = (state = INITIAL_STATE, action) => {
    console.log(action);
    switch(action.type){
        case 'LOGIN' : {
            return {
                ...state, token: action.token
            }
        }
    }
    return state;
}

export default userReducer;