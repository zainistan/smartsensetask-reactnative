import axios from "axios";

export default axios.create({
  baseURL: "https://ssbdcloud.devalertlist.com/api",
  responseType: "json"
});